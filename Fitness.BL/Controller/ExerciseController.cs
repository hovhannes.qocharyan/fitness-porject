﻿using Fitness.BL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitness.BL.Controller
{
    public class ExerciseController : ControllerBase
    {
        private const string EXER_FILE_NAME = "exercises.dat";
        private const string ACTIV_FILE_NAME = "activities.dat";
        private readonly User user;
        public List<Exercise> Exercises { get; }
        public List<Activity> Activities { get; }
        public ExerciseController(User user)
        {
            this.user = user ?? throw new ArgumentNullException("User cant be null", nameof(user));
            Exercises = GetAllExercises();
            Activities = GetActivities();
        }

        private List<Activity> GetActivities()
        {
            return Load<List<Activity>>(ACTIV_FILE_NAME) ?? new List<Activity>();
        }

        public  void Add(Activity activity , DateTime begin ,DateTime end)
        {
            var act = Activities.SingleOrDefault(a => a.Name == activity.Name);
            if (act == null)
            {
                Activities.Add(activity);
                var exeecise = new Exercise(begin, end, activity, user);
                Exercises.Add(exeecise);
                
            }
            else
            {
                var exeecise = new Exercise(begin, end, act, user);
                Exercises.Add(exeecise);
                
            }
            Save();

        }
        private List<Exercise> GetAllExercises()
        {
            return Load < List<Exercise>>(EXER_FILE_NAME) ?? new List<Exercise>();
        }
        private void Save()
        {
            Save(EXER_FILE_NAME, Exercises);
            Save(ACTIV_FILE_NAME, Activities);
        }
      
    }

}
