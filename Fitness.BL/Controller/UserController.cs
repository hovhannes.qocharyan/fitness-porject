﻿using Fitness.BL.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Linq;

namespace Fitness.BL.Controller
{
    /// <summary>
    /// User Controller
    /// </summary>
    public class UserController :ControllerBase
    {
        /// <summary>
        /// User who using controller
        /// </summary>
        /// 
        private const string _path = "users.dat";
        public List<User> Users { get; }
        public User CurrentUser { get; }
        public bool IsNewUser { get; } = false;
        /// <summary>
        /// Creating new Controller
        /// </summary>
        /// <param name="user"></param>
        public UserController(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new ArgumentNullException("Name cant be null", nameof(userName));
            }
            Users = GetUsersDtat();
            CurrentUser = Users.SingleOrDefault(u => u.Name == userName);
            if (CurrentUser == null)
            {
                CurrentUser = new User(userName);
                IsNewUser = true;
                Users.Add(CurrentUser);

                Save();
            }



        }



        /// <summary>
        /// Get saved list of users
        /// </summary>
        /// <returns>List<Users></returns>
        private List<User> GetUsersDtat()
        {
           return Load < List<User>>(_path)??new List<User>();
            

        }

        private object List<T>(string v)
        {
            throw new NotImplementedException();
        }

        public void SetNewUserData(string genderName , DateTime birthDay   , double weight=1,double height = 1)
        {
            if (string.IsNullOrWhiteSpace(genderName))
            {
                throw new ArgumentNullException("Gender cant be null", nameof(genderName)); 
            }
            if(birthDay == null)
            {
                throw new ArgumentNullException("BirthDay cant be null", nameof(birthDay));
            }
            if (weight <= 0)
            {
                throw new ArgumentException("Invalid argument", nameof(weight));
            }
            if (height <= 0)
            {
                throw new ArgumentException("Invalid argument", nameof(height));
            }
            CurrentUser.Gender = new Gender(genderName);
            CurrentUser.BirtDay = birthDay;
            CurrentUser.Weight = weight;
            CurrentUser.Height = height;
            Save();

        }

        /// <summary>
        /// Save data
        /// </summary>
        public void Save()
        {
            Save(_path, Users);
        }
        /// <summary>
        /// Load data
        /// </summary>
        /// <returns>User</returns>
    }
}


