﻿using Fitness.BL.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Fitness.BL.Controller
{
    public class EatingCantroller:ControllerBase
    {
        private const string _path = "foods.dat";
        private const string _eatingsFileName = "eatings.dat";
        private readonly User user;
        public List<Food> Foods { get; }
        public Eating Eating { get; }
        public EatingCantroller(User user)
        {
            this.user = user ?? throw new ArgumentNullException("User cant be null", nameof(user));
            Foods = GetAllFoods();
            Eating = GetEating();
        }
       
        public void Add(Food food ,double weight)
        {
            var product = Foods.SingleOrDefault(f => f.Name == food.Name); 
            if(product == null)
            {
                Foods.Add(food);
                Eating.Add(food, weight);
                Save();
            }
            else
            {
                Eating.Add(product, weight);
                Save();
            }
        }
        

        
        private Eating GetEating()
        {
            return Load<Eating>(_eatingsFileName) ?? new Eating(user);
        }

        private List<Food> GetAllFoods()
        {
            return Load<List<Food>>(_path) ?? new List<Food>();
        }
        private void Save()
        {
            Save(_path, Foods);
            Save(_eatingsFileName , Eating);
        }
        
    }
}
