﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fitness.BL.Model
{
    /// <summary>
    /// User
    /// </summary>
    /// 
    [Serializable]
    public class User
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name {get;}
        /// <summary>
        /// Gender
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// BirthDay
        /// </summary>
        public DateTime BirtDay { get; set; }
        /// <summary>
        /// Weight
        /// </summary>
        public double Weight { get; set; }
        /// <summary>
        /// Height
        /// </summary>
        public double Height { get; set; }
        /// <summary>
        /// Creating new User
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="gender">Gender</param>
        /// <param name="bD">BirthDay</param>
        /// <param name="weight">Weight</param>
        /// <param name="height">Height</param>
        public User(string name , Gender gender , DateTime bD , double weight , double height)
        {
            #region Checing Argumnets

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("User name cant be null or withspace" , nameof(name));
            }

            if(gender == null)
            {
                throw new ArgumentNullException("User gender cant be null" , nameof(gender));
            }
            if (bD < DateTime.Parse("01.01.1900") || bD >= DateTime.Now)
            {
                throw new ArgumentException("Invalid datetime for birthday" , nameof(bD));
            }
            if (weight <= 0)
            {
                throw new ArgumentException("Invalid weight" , nameof(weight));
            }
            if (height <= 0)
            {
                throw new ArgumentException("Inavlid height" , nameof(height));
            }
#endregion

            Name = name;
            Gender = gender;
            BirtDay = bD;
            Weight = weight;
            Height = height;

        }
        public User(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("Name cant be null", nameof(name));
            }
            Name = name;
        }
        public int Age { get { return DateTime.Now.Year - BirtDay.Year; } }
        public override string ToString()
        {
            return Name + " " + Age;
        }
    }
}
