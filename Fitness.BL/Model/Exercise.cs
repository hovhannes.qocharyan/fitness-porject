﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fitness.BL.Model
{
    [Serializable]
    public class Exercise
    {
        public DateTime Start { get;  }
        public DateTime Finish { get;  }
        public Activity Activity { get;  }
        public User User { get; }

        public Exercise(DateTime start ,DateTime finish ,Activity activity ,User user)
        {
            if(start > finish)
            {
                throw new ArgumentException("Start date cant be bigger then finish date time");
            }
            if(start == null || finish == null)
            {
                throw new ArgumentException("Time cant be Null");
            }
            if (activity == null)
            {
                throw new ArgumentNullException("Activity cant be null", nameof(activity));
            }
            if (user == null)
            {
                throw new ArgumentNullException("User cant be null", nameof(user));
            }
            Start = start;
            Finish = finish;
            Activity = activity;
            User = user;
        }
    }
}
