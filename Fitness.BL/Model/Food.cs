﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fitness.BL.Model
{
    [Serializable]
   public class Food
    {
        public string Name { get; }
        public double Callories { get; }

        /// <summary>
        /// Protein
        /// </summary>
        public double Proteins { get; set; }
        /// <summary>
        /// Fat
        /// </summary>
        public double Fats { get;  }
        /// <summary>
        /// Carbohydrate
        /// </summary>
        public double Carbohydrates { get; }
        /// <summary>
        /// Calories for 100 gram
        /// </summary>

        public double Calories { get; }
        


        public Food(string name) : this(name ,0,0,0,0)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("Food name cant be null or whit Space", nameof(name));
            }
            Name = name;
        }

        public Food(string name, double callories , double proteins ,double fats , double carbohydrates)
        {
            Name = name;
            Callories = callories/100.0;
            Proteins = proteins/100.0;
            Fats = fats/100.0;
            Carbohydrates = carbohydrates/100.0;
        }


        public override string ToString()
        {
            return Name;
        }
    }
}
