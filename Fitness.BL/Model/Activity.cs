﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fitness.BL.Model
{
    [Serializable]
    public class Activity
    {
        public string  Name { get;}
        public double CaloriesPerMinute { get; }

        public Activity(string name , double caloriesPerMinute)
        {
            if(string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("Activity name cant be null", nameof(name));
            }
            if(caloriesPerMinute <= 0)
            {
                throw new ArgumentException("invalid argument", nameof(caloriesPerMinute));
            }
            Name = name;
            CaloriesPerMinute = caloriesPerMinute;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
