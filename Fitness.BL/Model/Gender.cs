﻿using System;


namespace Fitness.BL.Model
{
    /// <summary>
    /// Gender.
    /// </summary>
    /// 
    [Serializable]
    public class Gender
    {
        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Creating new gender
        /// </summary>
        /// <param name="gender"></param>
        public Gender(string gender)
        {
           if (string.IsNullOrWhiteSpace(gender))
            {
                throw new ArgumentNullException("Invalid argument" + " " + nameof(Name));
            }
            
            Name = gender;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
