﻿
using Fitness.BL.Controller;
using Fitness.BL.Model;
using System;
using System.Globalization;
using System.Resources;

namespace Fitness.CMD 
{
    class Program
    {
       
        static void Main(string[] args)
        {

            var culture = CultureInfo.CreateSpecificCulture("en-us");
            var resourceManager = new ResourceManager("Fitness.CMD.Languages.Messages",typeof(Program).Assembly);
            Console.WriteLine(resourceManager.GetString("Hello",culture));
            Console.WriteLine(resourceManager.GetString("EnterName",culture));
            var name = Console.ReadLine();


            var userController = new UserController(name);
            var eatingCotroller = new EatingCantroller(userController.CurrentUser);
            var exerciseController = new ExerciseController(userController.CurrentUser);
            if (userController.IsNewUser)
            {
                Console.WriteLine("Enter your gander");
                var gender = Console.ReadLine();
                DateTime bD = ParseDateTime("birthDay");
                double weight = ParseDouble("weight");
                double height = ParseDouble("height");
                userController.SetNewUserData(gender, bD, weight, height);
            }

            while (true)
            {


                Console.WriteLine(userController.CurrentUser);
                Console.WriteLine("What are u want do");
                Console.WriteLine("E - enter the eating");
                Console.WriteLine("A -ender the train");
                Console.WriteLine("Q - exit");

                var key = Console.ReadKey();

                switch (key.Key)
                {
                    case ConsoleKey.E:
                        var foods = EnterEating();
                        eatingCotroller.Add(foods.Food, foods.Weight);
                        foreach (var item in eatingCotroller.Eating.Foods)
                        {
                            Console.WriteLine($"{item.Key} , {item.Value}");
                        }
                        break;
                    case ConsoleKey.A:
                        var exe = EnterExecise();
                        exerciseController.Add(exe.Activity, exe.Begin,exe.Finish );
                        foreach (var item in exerciseController.Exercises)
                        {
                            Console.WriteLine($"{item.Activity} start {item.Start.ToShortTimeString()} end {item.Finish.ToShortTimeString()}");
                        }

                        break;

                    case ConsoleKey.Q:
                        Environment.Exit(0);
                        break;

                }
            }
            Console.ReadKey();
            

        }

        private static (DateTime Begin , DateTime Finish ,Activity Activity) EnterExecise()
        {
            Console.WriteLine("Enter the name of  exercise");
            var name = Console.ReadLine();
            var energy = ParseDouble("CaloriesBurningInMinute");
            var activity = new Activity(name, energy);
            var begin = ParseDateTime("Begin");
            var finish = ParseDateTime("Finish");
            return (begin, finish, activity);
           
        }

        private static (Food  Food , double Weight) EnterEating()
        {
            Console.WriteLine("Enter the name of product");
            var food = Console.ReadLine();

            var calories = ParseDouble("Calories");
            var prot = ParseDouble("Proteins");
            var fats = ParseDouble("Fats");
            var carbs = ParseDouble("Carbohydrates");
            var weight = ParseDouble("Weight of food");
            var product = new Food(food,calories,prot,fats,carbs);
            return (product,weight);
           
           
        }

        private static DateTime ParseDateTime(string value)
        {
            DateTime bD;
            while (true)
            {
                Console.WriteLine($"Enter {value} (dd-MM-yyy) ");
                if (DateTime.TryParse(Console.ReadLine(), out bD))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid format or argument");
                }
            }

            return bD;
        }

        private static double ParseDouble(string value)
        {
            while (true)
            {
                Console.WriteLine($"Enter your {value} ");
                if (double.TryParse(Console.ReadLine(), out double result))
                {
                    return result;
                }
                else
                {
                    Console.WriteLine("Invalid argument");
                }
            }
        }
    }
}
