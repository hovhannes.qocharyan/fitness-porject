using Fitness.BL.Controller;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject1
{
    [TestClass]
    public class UserControllerTests
    {
        //[TestMethod]
        //public void UserControllerTest()
        //{

        //}
        [TestMethod]
        public void SetNewUserDataTest()
        {
            var userName = Guid.NewGuid().ToString();
            var birthDay = DateTime.Now.AddYears(-18);
            var weight = 90;
            var height = 180;
            var gender = "man";

            var controller = new UserController(userName);
            controller.SetNewUserData(gender, birthDay, weight, height);
            var controller2 = new UserController(userName);


            Assert.AreEqual(userName, controller2.CurrentUser.Name);
            Assert.AreEqual(birthDay, controller2.CurrentUser.BirtDay);
            Assert.AreEqual(weight, controller2.CurrentUser.Weight);
            Assert.AreEqual(height, controller2.CurrentUser.Height);
            Assert.AreEqual(gender, controller2.CurrentUser.Gender.Name);

        }
        [TestMethod]
        public void SaveTest()
        {
            // Arrange
            var userName = Guid.NewGuid().ToString();
            // Act
            var controller = new UserController(userName);
            //Assert
            Assert.AreEqual(userName, controller.CurrentUser.Name);

        }
    }
}
