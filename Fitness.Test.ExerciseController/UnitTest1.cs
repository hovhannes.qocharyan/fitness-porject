using Fitness.BL.Controller;
using Fitness.BL.Model;
using System;
using System.Linq;
using Xunit;

namespace Fitness.Test.ExerciseController
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Arrenge
            var userName = Guid.NewGuid().ToString();
            var foodName = Guid.NewGuid().ToString();
            var rnd = new Random();
            var userController = new UserController(userName);
            var eatingController = new EatingCantroller(userController.CurrentUser);
            var food = new Food("foodName", rnd.Next(50, 500), rnd.Next(50, 500), rnd.Next(50, 500), rnd.Next(50, 500));

            //Act
            eatingController.Add(food, 100);

            //Assert
            Assert.AreEqual(food.Name, eatingController.Eating.Foods.First().Key.Name);
            Assert.AreEq


        }
    }
}
