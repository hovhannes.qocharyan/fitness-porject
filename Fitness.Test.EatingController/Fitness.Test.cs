using Fitness.BL.Controller;
using Fitness.BL.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fitness.Test.EatingController
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void AddTest()
        {
            //Arrenge
            var userName = Guid.NewGuid().ToString();
            var foodName = Guid.NewGuid().ToString();
            var rnd = new Random();
            var userController = new UserController(userName);
            var eatingController = new EatingCantroller(userController.CurrentUser);
            var food = new Food("foodName", rnd.Next(50, 500), rnd.Next(50, 500), rnd.Next(50, 500), rnd.Next(50, 500));

            //Act
            eatingController.Add(food, 100);

            //Assert
            Assert.AreEqual(food.Name, eatingController.Eating.Foods.First().Key.Name);
        


        }
        [TestMethod]
        public void SaveLoad()
        {
            DateTime bd = new DateTime(1995,01,01);
            var gender = new Gender("man");
            var user = new User("user", gender, bd, 80, 170);
            var usercontroller = new UserController("user");
            var eatingController = new EatingCantroller(user);
            var food = new Food("meet", 500, 300, 150, 50);
            eatingController.Add(food, 300);
            var eatingController1 = new EatingCantroller(user);
           List<Food> list =  eatingController.Foods;


            Assert.AreEqual(food, list[0]);

            
           
        }
    }
}
